package com.wattwurm.toodoo.model

import java.util.Calendar

class Task {
    // id is assigned when task read form storage (then it has an id) or created (then it gets nextId)
    var id: Int = -1
    var name: String = ""
    var priority = Prio.LOW
    var status = CompletionStatus.OPEN
    var dueDate: TDate? = null
    var dueTime: TTime? = null
    lateinit var category: Category
    var createDate: Calendar = Calendar.getInstance()
    var lastModifiedDate: Calendar = Calendar.getInstance()
    var desc: String = ""

    fun displayInLog(): String {
        val dueDateS = dueDate?.toStorageRep() ?: ""
        val dueTimeS = dueTime?.toStorageRep() ?: ""
        val catIdS = category.id.toString()
        val createDateS = convertCalndrForXML(createDate)
        return "${id}\t${name}\t${priority.rep}\t${status.symbol}\t${dueDateS}\t${dueTimeS}\t${catIdS}\t${createDateS}"
    }
}