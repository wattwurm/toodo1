package com.wattwurm.toodoo.model

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

private val formatCalndrForXML = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ")
fun convertCalndrForXML(cal: Calendar): String = formatCalndrForXML.format(cal.time)
fun convertStringFromXMLToDate(str: String): Date? = formatCalndrForXML.parse(str)

private val formatCalndrForDisplay = SimpleDateFormat("yyyy-MM-dd HH:mm:ss" )
fun convertCalndrForDisplay(cal: Calendar): String = formatCalndrForDisplay.format(cal.time).replace(" ", "\u00A0")

private val formatCalndrForFileName = SimpleDateFormat("yyyy-MM-dd-HH-mm-ss" )
fun convertCalndrForFileName(cal: Calendar): String = formatCalndrForFileName.format(cal.time)
