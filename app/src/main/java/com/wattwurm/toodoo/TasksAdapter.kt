package com.wattwurm.toodoo

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wattwurm.toodoo.data.Tasks
import com.wattwurm.toodoo.databinding.TaskItemRBinding
import com.wattwurm.toodoo.model.CompletionStatus
import com.wattwurm.toodoo.model.Prio
import com.wattwurm.toodoo.model.TDate
import com.wattwurm.toodoo.model.TTime
import com.wattwurm.toodoo.model.Task

class TasksAdapter(private val tasks: Tasks) : RecyclerView.Adapter<TasksAdapter.ItemViewHolderR>(){

    class ItemViewHolderR(val binding: TaskItemRBinding) : RecyclerView.ViewHolder(binding.root)

    override fun getItemCount(): Int {
        return tasks.countSelectedTasks
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolderR {
        val binding = TaskItemRBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolderR(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolderR, position: Int) {
        val currentTask = tasks.itemAtPosition(position)
        holder.binding.taskName.text = currentTask.name
        holder.binding.taskName.paintFlags = when (currentTask.status) {
            CompletionStatus.DONE -> holder.binding.taskName.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            else -> holder.binding.taskName.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
        }

        holder.binding.catName.text = currentTask.category.name
        holder.binding.date.text = currentTask.dueDate?.displayAs_DDD_MMM_DD ?: ""
        holder.binding.timeOrYear.text = currentTask.contentOfTimeOrYear
        holder.binding.imagePrio.setImageResource(tasks.itemAtPosition(position).prioImage)
        holder.binding.root.setBackgroundResource(currentTask.backgroundColorInTaskList)

        holder.binding.root.setOnCreateContextMenuListener { contextMenu, _, _ ->
            tasks.currentItemPos = position
        }
    }

}

val Task.contentOfTimeOrYear : String get() {
    this.dueDate?.let {
        if(it.isMoreThanOneYearFromToday()) {
            return it.year.toString()
        }
    }
    return this.dueTime?.toStorageRep() ?: ""
}

val Task.prioImage: Int get() = when (this.priority) {
    Prio.LOW -> R.drawable.circle_green
    Prio.MEDIUM -> R.drawable.circle_yellow
    Prio.HIGH -> R.drawable.circle_red
}

val Task.backgroundColorInTaskList: Int get() {
    if (this.status == CompletionStatus.DONE) return R.drawable.taskbg_completed
    val dueDate = this.dueDate
    val dueTime = this.dueTime
    if (dueDate != null) {
        if(dueDate.isLessThan(TDate.today)) return R.drawable.taskbg_overdue
        if(dueDate == TDate.today) {
            if (dueTime != null) {
                if(dueTime.compareTo(TTime.now) <= 0) return R.drawable.taskbg_overduetoday
            }
            return R.drawable.taskbg_today
        }
    }
    return R.drawable.taskbg_future
}
