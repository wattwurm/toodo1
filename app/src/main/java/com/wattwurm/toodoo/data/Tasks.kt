package com.wattwurm.toodoo.data

import com.wattwurm.toodoo.model.Category
import com.wattwurm.toodoo.model.TDate
import com.wattwurm.toodoo.model.Task
import java.util.Calendar
import kotlin.Comparator

class Tasks {

    private val allTasks = mutableListOf<Task>()
    private val selectedTasks = mutableListOf<Task>()

    var currentItemPos = -1

    private var nextId = 0
    fun getNextId() : Int {
        nextId++
        return nextId
    }
    fun resetNextId() {
        nextId = 0
    }

    var filters = Filters()

    val sortOrder = SortCrit.defaultSortOrder.toMutableList()

    private val taskComparator = Comparator<Task> { t1, t2 ->
        fun taskCompare(t1: Task, t2: Task): Int {
            sortOrder.forEach { sortCrit ->
                sortCrit.compare(t1, t2).let { if(it != 0) return it }
            }
            return 0
        }
        taskCompare(t1,t2)
    }

    var listMode = ListMode.FILTERED_LIST
    var listModePending = ListMode.FILTERED_LIST
    lateinit var currentCategory : Category
    var searchCrit = SearchCrit("", false, SearchScope.NAME_AND_DESC)

    val countAllTasks get() = allTasks.size
    val countSelectedTasks get() = selectedTasks.size

    // precondition: currentCatPos > -1
    val currentTask: Task
        get() {
            return selectedTasks[currentItemPos]
        }

    fun itemAtPosition(pos: Int): Task {
        return selectedTasks[pos]
    }

    val iterator get() = allTasks.iterator()

    fun setCreateItemPending() {
        currentItemPos = -1
    }

    fun addTask(task: Task) {
        task.id = getNextId()
        allTasks.add(task)
        if(criteriaApplyToTask(task)) {
            // first check if current criteria apply
            selectedTasks.add(task)
            sortSelected()
        }
    }

    fun setTasksFromStorage(taskList: List<Task>) {
        allTasks.clear()
        allTasks.addAll(taskList)
//        refreshSelectedTasks()
        nextId = allTasks.map { it.id }.maxOrNull() ?: 0
    }

    fun removeCurrentItem() {
        val current = this.currentTask
        selectedTasks.removeAt(currentItemPos)
        allTasks.remove(current)
        currentItemPos = -1
    }

    fun tasksExistingForCategory(cat: Category) = allTasks.any { it.category == cat }

    fun tasksWithNameExisting(name: String) = allTasks.any { it.name == name }

    fun numberOfTasksForCategory(cat: Category) = allTasks.count { it.category == cat }

    // different criteria needed, depending on list mode
    private fun criteriaApplyToTask(task: Task) : Boolean {
        return when (this.listMode) {
            ListMode.FILTERED_LIST -> filters.applyToTask(task)
            ListMode.STRING_SEARCH -> searchCrit.applyToTask(task)
            ListMode.SINGLE_CATEGORY -> currentCategory == task.category
        }
    }

    fun onTaskChanged() {
        currentTask.lastModifiedDate = Calendar.getInstance()
        if(!criteriaApplyToTask(currentTask)) {
            selectedTasks.removeAt(currentItemPos)
            currentItemPos = -1
        } else {
            sortSelected()
        }
    }

    fun applyPendingListMode() {
        if(listModePending != listMode) {
            listMode = listModePending
            refreshSelectedTasks()
        }
    }

    fun refreshSelectedTasks() {
        selectedTasks.clear()
        selectedTasks.addAll(allTasks.filter { criteriaApplyToTask(it) })
        sortSelected()
    }

    private fun sortSelected() {
        selectedTasks.sortWith(taskComparator)
    }

    fun setSortOrder(newOrder: List<SortCrit>) {
        sortOrder.clear()
        sortOrder.addAll(newOrder)
        sortSelected()
    }

    val singleCategoryIfExisting get() = when (this.listMode) {
        ListMode.FILTERED_LIST -> filters.catFilter.let {
            if (it is MultipleFilterM && it.options.size == 1) it.options[0] else null
        }
        ListMode.STRING_SEARCH -> null
        ListMode.SINGLE_CATEGORY -> currentCategory
    }

    val defaultDateForNewTask: TDate
        get() {
        return when (listMode) {
            ListMode.FILTERED_LIST -> filters.defaultDateForNewTask
            else -> TDate.today
        }
    }

    val firstDueDate: TDate? get() {
        return allTasks.mapNotNull { it.dueDate }.minOrNull()
    }
    val lastDueDate: TDate? get() {
        return allTasks.mapNotNull { it.dueDate }.maxOrNull()
    }

//    private val filterDateIsBeforeInAfterTaskDateRangeOld : Int get() {
//        return when (listMode) {
//            ListMode.FILTERED_LIST ->  {
//                val taskDateRangeFrom = (firstDueDate ?: TDate.today).let { if (it < TDate.today) it else TDate.today }
//                val taskDateRangeTo = (lastDueDate ?: TDate.today).let { if (it > TDate.today) it else TDate.today }
//                filters.filterDateIsBeforeInAfterDateRange(taskDateRangeFrom, taskDateRangeTo)
//            }
//            else -> 0
//        }
//    }

//    val filterDateIsBeforeInAfterTaskDateRange : Int get() {
//        val taskDateRangeFrom = (firstDueDate ?: TDate.today).let { if (it < TDate.today) it else TDate.today }
//        val taskDateRangeTo = (lastDueDate ?: TDate.today).let { if (it > TDate.today) it else TDate.today }
//        return filters.filterDateIsBeforeInAfterDateRange(taskDateRangeFrom, taskDateRangeTo)
//    }

}


data class SearchCrit (val sstring: String, val caseSensitive: Boolean, val scopeOption: SearchScope) {
    fun applyToTask(task: Task) =
        (scopeOption.searchName && task.name.contains(sstring, ignoreCase = !caseSensitive))
                || (scopeOption.searchDesc && task.desc.contains(sstring, ignoreCase = !caseSensitive))
}

enum class SearchScope {
    NAME_AND_DESC {
        override val searchName = true
        override val searchDesc = true
    },
    NAME_ONLY {
        override val searchName = true
        override val searchDesc = false
    },
    DESC_ONLY {
        override val searchName = false
        override val searchDesc = true
    };

    abstract val searchName: Boolean
    abstract val searchDesc: Boolean
}

enum class ListMode {
    FILTERED_LIST, STRING_SEARCH, SINGLE_CATEGORY;

    val isSecondLevelList get() = when (this) {
        FILTERED_LIST -> false
        STRING_SEARCH -> true
        SINGLE_CATEGORY -> true
    }
}
