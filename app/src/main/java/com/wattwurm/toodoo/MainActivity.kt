package com.wattwurm.toodoo

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.wattwurm.toodoo.data.AppState
import com.wattwurm.toodoo.data.readCategoriesFromStream
import com.wattwurm.toodoo.data.readFiltersFromStream
import com.wattwurm.toodoo.data.readSortCritFromStream
import com.wattwurm.toodoo.data.readTasksFromStream
import com.wattwurm.toodoo.data.writeAllDataToStream
import com.wattwurm.toodoo.data.writeCategoriesToStream
import com.wattwurm.toodoo.data.writeFiltersToStream
import com.wattwurm.toodoo.data.writeSortCritToStream
import com.wattwurm.toodoo.data.writeTasksToStream
import com.wattwurm.toodoo.databinding.ActivityMainBinding
import com.wattwurm.toodoo.model.CompletionStatus
import com.wattwurm.toodoo.model.Prio
import com.wattwurm.toodoo.model.TDate
import com.wattwurm.toodoo.model.TTime
import com.wattwurm.toodoo.model.Task
import com.wattwurm.toodoo.model.convertCalndrForFileName
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.util.Calendar


class MainActivity : AppCompatActivity() {

    private val request_external_storage = 17  // any number can be used, simply must be the same in requestPermissions and onRequestPermissionsResult

    private val fileNameTasks = "tasks.xml"
    private val fileNameCategories = "categories.xml"
    private val fileNameFilters = "filters.xml"
    private val fileNameSortCrit = "sorts.xml"

    // todo   is there a better way to access data from activities and fragments, e.g. ViewModel
    val appState = AppState()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        readCategories()
        readTasks()
        readFilters()
        readSortCrit()

        if (appState.categories.countItems == 0 && appState.tasks.countAllTasks == 0) {
            createExampleDataForStart()
            writeCategories()
            writeTasks()
            Toast.makeText(
                this.applicationContext,
                "toodoo app initial start",
                Toast.LENGTH_LONG).show()
            Toast.makeText(
                this.applicationContext,
                "created:\n${appState.categories.countItems} example categories \n" +
                        "${appState.tasks.countAllTasks} example tasks",
                Toast.LENGTH_LONG).show()
        }

        val binding = ActivityMainBinding.inflate(layoutInflater)
        //setContentView(R.layout.activity_main)
        setContentView(binding.root)

        showTaskList()
    }

    fun writeCategories() {
        Log.i("DEBUG", "writeCategories start")
        var fos : FileOutputStream? = null
        try {
            fos = openFileOutput(fileNameCategories, Context.MODE_PRIVATE)
            appState.writeCategoriesToStream(fos)
        } catch (e: Exception) {
            val message = "error saving categories - Exception $e "
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        } finally {
            fos?.close()
        }
    }

    private fun readCategories() {
        Log.i("DEBUG", "readCategories start")
        var fis : FileInputStream? = null
        try {
            fis = this.openFileInput(fileNameCategories)
            appState.readCategoriesFromStream(fis)
            // Toast.makeText(applicationContext, "${appState.categories.countItems} categories read from xml", Toast.LENGTH_SHORT).show()
        } catch (e: FileNotFoundException) {
            Log.i("DEBUG", "error reading categories, file $fileNameCategories not found, $e")
        } catch (e: Exception) {
            val message = "error reading categories: $e"
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        } finally {
            fis?.close()
        }
    }

    private fun readTasks() {
        Log.i("DEBUG", "readTasks start")
        var fis : FileInputStream? = null
        try {
            fis = this.openFileInput(fileNameTasks)
            appState.readTasksFromStream(fis)
        } catch (e: FileNotFoundException) {
            Log.i("DEBUG", "error reading tasks, file $fileNameCategories not found, $e")
        } catch (e: Exception) {
            val message = "error reading tasks: $e"
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        } finally {
            fis?.close()
        }
    }

    private fun readFilters() {
        Log.i("DEBUG", "readFilters start")
        var fis : FileInputStream? = null
        try {
            fis = this.openFileInput(fileNameFilters)
            appState.readFiltersFromStream(fis)
        } catch (e: FileNotFoundException) {
            Log.i("DEBUG", "error reading filters, file $fileNameFilters not found, $e")
        } catch (e: Exception) {
            val message = "error reading filters: $e"
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        } finally {
            fis?.close()
        }
    }

    private fun readSortCrit() {
        Log.i("DEBUG", "readSortCrit start")
        var fis : FileInputStream? = null
        try {
            fis = this.openFileInput(fileNameSortCrit)
            appState.readSortCritFromStream(fis)
        } catch (e: FileNotFoundException) {
            Log.i("DEBUG", "error reading sort criteria, file $fileNameSortCrit not found, $e")
        } catch (e: Exception) {
            val message = "error reading sort criteria: $e"
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        } finally {
            fis?.close()
        }
    }

        private fun createExampleDataForStart() {

        // some categories
        with (appState.categories) {
            addCategoryWithName("toodooInstructions")
            addCategoryWithName("Family")
            addCategoryWithName("Friends")
            addCategoryWithName("Garden")
            addCategoryWithName("House")
            addCategoryWithName("Travel")
        }

        val exampleDescription = "This is an example task, created automatically by toodoo." +
                " \nYou can change it, delete it, use it to learn about handling the toodoo app, " +
                "and you can create new tasks of your own (use the symbol + in the action bar)."

        // some tasks
        val taskLearnToodoo = Task()
        taskLearnToodoo.name = "Acquaint yourself with toodoo"
        taskLearnToodoo.category = appState.categories.categoryForName("toodooInstructions")
        taskLearnToodoo.desc = "Filtering: " +
                "\nBy default, the task list displays ALL tasks - " +
                "no matter if open or done, high or low priority, due today, next week or overdue. " +
                "\nYou can restrict the tasks to be displayed, " +
                "e. g. by selecting open tasks only, or only tasks with high priority, or tasks due this week (or any combination of these)." +
                "\nClick on the filter symbol in the action bar and try it." +
                "\n\n" +
                "Sorting: " +
                "\nBy default, the task list is sorted by " +
                "\n* dueDate \n* priority \n* category \n* taskName \n* completionStatus. " +
                "\nYou can change the sort order, e. g. if you prefer to have all high priority tasks first, " +
                "or sort tasks by category, or alphabetically by task name." +
                "\nClick on \"set sort criteria\" in the options menu and change the sort order according to your needs." +
                "\n\n" +
                "Categories: " +
                "\nEach task belongs to a category. " +
                "Categories can be created and deleted as needed." +
                "\nClick on \"manage categories\" in the options menu in order to create / delete categories according to your needs." +
                "\nThe name of a category can be changed, which will then be visible in all tasks of that category." +
                "\nNote: a category can only be deleted if it has no tasks."
        taskLearnToodoo.priority = Prio.HIGH
        taskLearnToodoo.dueDate = TDate.today
        appState.tasks.addTask(taskLearnToodoo)

        val task1 = Task()
        task1.name = "birthday party aunt Peggy"
        task1.category = appState.categories.categoryForName("Family")
        task1.desc = exampleDescription
        task1.dueDate = TDate.today.addDays(-3)
        task1.dueTime = TTime(15, 30)
        task1.priority = Prio.LOW
        appState.tasks.addTask(task1)

        val task2 = Task()
        task2.name = "wedding Jill & Jake"
        task2.category = appState.categories.categoryForName("Friends")
        task2.desc = exampleDescription
        task2.priority = Prio.MEDIUM
        task2.dueDate = TDate.today.addDays(1)
        task2.dueTime = TTime(19, 0)
        appState.tasks.addTask(task2)

        val task3 = Task()
        task3.name = "fertilize rhubarb"
        task3.category = appState.categories.categoryForName("Garden")
        task3.desc = exampleDescription
        task3.priority = Prio.LOW
        task3.dueDate = TDate.today.addDays(-2)
        task3.status = CompletionStatus.DONE
        appState.tasks.addTask(task3)

        val task5 = Task()
        task5.name = "water flowers"
        task5.category = appState.categories.categoryForName("Garden")
        task5.desc = exampleDescription
        task5.priority = Prio.MEDIUM
        task5.dueDate = TDate.today.addDays(3)
        appState.tasks.addTask(task5)

        val task6 = Task()
        task6.name = "trip to Athens"
        task6.category = appState.categories.categoryForName("Travel")
        task6.desc = exampleDescription
        task6.priority = Prio.LOW
        appState.tasks.addTask(task6)
    }

    fun writeTasks() {
        Log.i("DEBUG", "writeTasks start")
        var fos : FileOutputStream? = null
        try {
            fos = openFileOutput(fileNameTasks, Context.MODE_PRIVATE)
            appState.writeTasksToStream(fos)
        } catch (e: Exception) {
            val message = "error saving tasks - Exception $e "
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        } finally {
            fos?.close()
        }
    }

    fun writeFilters() {
        Log.i("DEBUG", "writeFilters start")
        var fos : FileOutputStream? = null
        try {
            fos = openFileOutput(fileNameFilters, Context.MODE_PRIVATE)
            appState.writeFiltersToStream(fos)
        } catch (e: Exception) {
            val message = "error saving filters - Exception $e "
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        } finally {
            fos?.close()
        }
    }

    fun writeSortCrit() {
        Log.i("DEBUG", "writeSortCrit start")
        var fos : FileOutputStream? = null
        try {
            fos = openFileOutput(fileNameSortCrit, Context.MODE_PRIVATE)
            appState.writeSortCritToStream(fos)
        } catch (e: Exception) {
            val message = "error saving sort criteria - Exception $e "
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        } finally {
            fos?.close()
        }
    }

    fun showTaskList() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentTaskLst()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        // fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun showTaskListSecondLevel() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentTaskLst()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun showTaskDetail() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentDetail()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun showCatList() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentCategories()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun showFilters() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentFilters()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun showSortCrit() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentSortCriteria()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun checkAndWriteTasksToExt() {
        // val permission = ActivityCompat.checkSelfPermission(
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (permission == PackageManager.PERMISSION_GRANTED) {
            writeDataToExtFile()
        } else {
            // We don't have permission so prompt the user
            Log.i("DEBUG", "requesting permission from user ${Manifest.permission.WRITE_EXTERNAL_STORAGE}")
            Log.i("DEBUG", " request code 17")

            val stringArray = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            // requestPermissions(stringArray, request_external_storage);
            ActivityCompat.requestPermissions(
                this,
                stringArray,
                request_external_storage
            )
        }
    }

    private fun writeDataToExtFile() {
        Log.i("DEBUG", "writeDataToExtFile start")
        var fos : FileOutputStream? = null
        try {
            val downloadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val subdir = File(downloadFolder,"toodoo")
            if (!subdir.isDirectory) {  // also checks if existing
                subdir.mkdir()
            }
            val currentDateS = convertCalndrForFileName(Calendar.getInstance())
            val filename = "toodooData-${currentDateS}.xml"
            val file = File(subdir,filename)
            Log.i("DEBUG", "creating file ${file}")
            file.createNewFile()
            if(file.exists()) {
                fos = FileOutputStream(file)
                appState.writeAllDataToStream(fos)
                Log.i("DEBUG", "data exported to $file")
                Toast.makeText(applicationContext, "saved file\n$filename", Toast.LENGTH_LONG).show()
                Toast.makeText(applicationContext, "in folder\n${subdir}", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(applicationContext, "output to external file not possible, file could not be created ", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            val message = "error exporting data to external file - Exception $e "
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        } finally {
            fos?.close()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        // this is called when the requested permission is returned to this activity
        Log.i("DEBUG", "onRequestPermissionsResult start")
        Log.i("DEBUG", "requestCode is $requestCode")
        Log.i("DEBUG", "permissions size is ${permissions.size}")
        Log.i("DEBUG", "permissions first elem  is ${permissions[0]}")
        Log.i("DEBUG", "grantResults size is ${grantResults.size}")
        Log.i("DEBUG", "grantResults first elem  is ${grantResults[0]}")

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == request_external_storage) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                writeDataToExtFile()
            }
            else {
                Toast.makeText(applicationContext,
                    "Storage Permission Denied",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    fun createDialogAbout() {
        val builder = AlertDialog.Builder(this)
        builder
            //.setMessage("toodoo")
            .setView(layoutInflater.inflate(R.layout.dialog_about, null))  // results in warning null as root should be avoided - but in dialog not possible otherwise
            .setPositiveButton("OK") { dialog, id -> }
        //.setNegativeButton("Cancel") { dialog, id -> } // User cancelled the dialog
        val dialog = builder.create()
        dialog.show()
    }

}
