package com.wattwurm.toodoo.data

import com.wattwurm.toodoo.model.Task

enum class SortCrit {
    Completed {
        override fun compare(t1: Task, t2: Task): Int {
            // first completed, then open
            return  -(t1.status.compareTo(t2.status))
        }
    },
    DueDate {
        override fun compare(t1: Task, t2: Task): Int {
            val date1 = t1.dueDate
            val date2 = t2.dueDate

            return when(date1) {
                null -> when(date2) {
                    null -> 0
                    else -> 1
                }
                else -> when(date2) {
                    null -> -1
                    else -> {
                        date1.compareTo(date2).let {
                            if (it != 0) it
                            else {
                                // if dates are equal also consider dueTime
                                val time1 = t1.dueTime
                                val time2 = t2.dueTime
                                when(time1) {
                                    null -> when(time2) {
                                        null -> 0
                                        else -> 1
                                    }
                                    else -> when(time2) {
                                        null -> -1
                                        else -> time1.compareTo(time2)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        override val displayName = "Due Date/Time"
    },
    Priority {
        override fun compare(t1: Task, t2: Task): Int {
            // from high to low
            return -(t1.priority.compareTo(t2.priority))
        }
    },
    Category {
        override fun compare(t1: Task, t2: Task): Int {
            return  t1.category.compareTo(t2.category)
        }
    },
    Name {
        override fun compare(t1: Task, t2: Task): Int {
            return t1.name.compareTo(t2.name, ignoreCase = true)
        }
    };

    abstract fun compare(t1: Task, t2: Task): Int
    open val displayName: String = this.name

    companion object {
        // initially sort by DueDate, Prio, Category, Name, Completed
        val defaultSortOrder = listOf(
            DueDate,
            Priority,
            Category,
            Name,
            Completed
        )
    }

}