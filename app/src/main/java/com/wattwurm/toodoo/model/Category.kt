package com.wattwurm.toodoo.model

class Category (val id: Int, var name: String): Comparable<Category> {

    // toString determines what is displayed by ArrayAdapter in FragmentDetail
    override fun toString(): String {
        return name
    }

    fun displayInLog(): String {
        return "${id}\t${name}"
    }

    override fun compareTo(other: Category): Int {
        return this.name.toLowerCase().compareTo(other.name.toLowerCase())
    }
}