package com.wattwurm.toodoo.data

class AppState {

    val tasks = Tasks()
    val categories = Categories()
}
