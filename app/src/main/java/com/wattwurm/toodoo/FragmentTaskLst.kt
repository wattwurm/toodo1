package com.wattwurm.toodoo

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.ContextMenu
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.GestureDetectorCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
import com.wattwurm.toodoo.SwipeOnItemTouchListener.OnSwipeActionListener
import com.wattwurm.toodoo.data.DateFilter
import com.wattwurm.toodoo.data.FilterDirection
import com.wattwurm.toodoo.data.ListMode
import com.wattwurm.toodoo.data.SearchCrit
import com.wattwurm.toodoo.data.SearchScope
import com.wattwurm.toodoo.data.SingleFilter1
import com.wattwurm.toodoo.data.readAllDataFromStream
import com.wattwurm.toodoo.databinding.DialogSearchBinding
import com.wattwurm.toodoo.databinding.FragmentTaskLstBinding
import com.wattwurm.toodoo.model.CompletionStatus
import com.wattwurm.toodoo.model.TDate
import kotlin.math.abs

class FragmentTaskLst : Fragment() {

    private lateinit var act: MainActivity
    private lateinit var binding: FragmentTaskLstBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        Log.i("DEBUG", "FragmentTaskLst.onCreateView")
        act = this.activity as MainActivity

        Log.i("DEBUG", "ListMode old: ${act.appState.tasks.listMode} ")
        Log.i("DEBUG", "ListMode new: ${act.appState.tasks.listModePending} ")
        // there may be a pending list mode, so first reset tasks in list
        act.appState.tasks.applyPendingListMode()

        // Inflate the layout for this fragment
        binding = FragmentTaskLstBinding.inflate(inflater, container, false)
        binding.taskLstHeader.setText(textHeader())
        binding.taskLstNumTasks.setText(textNumTasks())

        val itemsRecyclerView = binding.taskListInFragment
        itemsRecyclerView.addItemDecoration(
            DividerItemDecoration(
                act,
                DividerItemDecoration.VERTICAL
            )
        )

        val recyclerViewAdapter = TasksAdapter(act.appState.tasks)
        itemsRecyclerView.adapter = recyclerViewAdapter
        // recyclerViewAdapter.notifyDataSetChanged()  // not needed, works anyway

        itemsRecyclerView.addOnItemTouchListener(
            SwipeOnItemTouchListener(act.applicationContext,
                // itemsRecyclerView,
                object : OnSwipeActionListener {
                    override fun onLeftSwipe() {
                        doSwipe(FilterDirection.FORWARD)
                    }
                    override fun onRightSwipe() {
                        doSwipe(FilterDirection.BACKWARD)
                    }
                }
            )
        )

        itemsRecyclerView.addOnItemClickListener { position: Int, view: View ->
            act.appState.tasks.currentItemPos = position
            act.showTaskDetail()
        }

        setHasOptionsMenu(true)
        registerForContextMenu(binding.taskListInFragment)  // needed for the context menu

        // handle back pressed for lists on second level, i.e. when list displays search results or tasks for one category
        act.onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(act.appState.tasks.listMode.isSecondLevelList) {
            override fun handleOnBackPressed() {
                act.appState.tasks.listModePending = ListMode.FILTERED_LIST
//                act.onBackPressed()       //  does NOT pop fragment from back stack, makes app freeze  :-(
//                requireActivity().onBackPressed()  // does NOT pop fragment from back stack either
                act.supportFragmentManager.popBackStack()
            }
        })

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_lst, menu)
        if(act.appState.tasks.listMode.isSecondLevelList) {
            menu.findItem(R.id.search).setVisible(false)
            menu.findItem(R.id.setFilter).setVisible(false)
            menu.findItem(R.id.setSortCrit).setVisible(false)
            menu.findItem(R.id.manageCategories).setVisible(false)
            menu.findItem(R.id.exportData).setVisible(false)
            menu.findItem(R.id.importData).setVisible(false)
//          menu.findItem(R.id.importTMLite).setVisible(false)
            if(act.appState.tasks.listMode == ListMode.STRING_SEARCH) {
                menu.findItem(R.id.new_task).setVisible(false)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.new_task -> {
                act.appState.tasks.setCreateItemPending()
                act.showTaskDetail()
                true
            }
            R.id.search -> {
                createDialogForSearch()
                true
            }
            R.id.setFilter -> {
                act.showFilters()
                true
            }
            R.id.setSortCrit -> {
                act.showSortCrit()
                true
            }
            R.id.exportData -> {
                act.checkAndWriteTasksToExt()
                true
            }
            R.id.importData -> {
                startIntentForImportDataRead()
                true
            }
//            R.id.importTMLite -> {
//                startIntentForFileReadFromTML()
//                true
//            }
            R.id.manageCategories -> {
                act.showCatList()
                true
            }
            R.id.about -> {
                act.createDialogAbout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        // menu.setHeaderTitle("Select what to do with task")
        act.menuInflater.inflate(R.menu.menu_tsk_ctxt, menu)
        when (act.appState.tasks.currentTask.status) {
            CompletionStatus.OPEN -> {
                menu.findItem(R.id.resetOpen).setVisible(false)
                menu.findItem(R.id.deleteTask).setVisible(false)
            }
            CompletionStatus.DONE -> {
                menu.findItem(R.id.setDone).setVisible(false)
            }
        }
        if(act.appState.tasks.currentTask.dueDate == TDate.today) {
            menu.findItem(R.id.setDueDateToday).setVisible(false)
        }
        if(act.appState.tasks.currentTask.dueDate == TDate.tomorrow) {
            menu.findItem(R.id.setDueDateTomorrow).setVisible(false)
        }
        if(act.appState.tasks.currentTask.dueTime == null) {
            menu.findItem(R.id.deleteTime).setVisible(false)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val taskName = act.appState.tasks.currentTask.name
        return when (item.itemId) {
            R.id.editTask -> {
                act.showTaskDetail()
                true
            }
            R.id.deleteTask -> {
                act.appState.tasks.removeCurrentItem()
                binding.taskListInFragment.adapter?.notifyDataSetChanged()
                // notifyItemRemoved instead notifyDataSetChanged is NOT sufficient,
                // because it does not change the positions of items further down in the list,
                // so accessing / editing a subsequent item may pick a wrong task
                binding.taskLstNumTasks.setText(textNumTasks())
                Toast.makeText(
                    this.context,
                    "task $taskName deleted",
                    Toast.LENGTH_LONG
                ).show()
                act.writeTasks()
                true
            }
            R.id.setDueDateToday -> {
                act.appState.tasks.currentTask.dueDate = TDate.today
                afterItemChanged()
                true
            }
            R.id.setDueDateTomorrow -> {
                act.appState.tasks.currentTask.dueDate = TDate.tomorrow
                afterItemChanged()
                true
            }
            R.id.deleteTime -> {
                act.appState.tasks.currentTask.dueTime = null
                afterItemChanged()
                true
            }
            R.id.setDone -> {
                act.appState.tasks.currentTask.status = CompletionStatus.DONE
                afterItemChanged()
                true
            }
            R.id.resetOpen -> {
                act.appState.tasks.currentTask.status = CompletionStatus.OPEN
                afterItemChanged()
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    private fun textHeader(): String {
        return when (act.appState.tasks.listMode) {
            ListMode.FILTERED_LIST -> act.appState.tasks.filters.displayInOverview.let { if (it.isNotBlank()) "tasks filtered by $it" else "all tasks" }
            ListMode.STRING_SEARCH -> "search result for: ${act.appState.tasks.searchCrit.sstring}"
            ListMode.SINGLE_CATEGORY -> "tasks for category: ${act.appState.tasks.currentCategory.name}"
        }
    }

    private fun textNumTasks(): String {
        val numberAllTasks = act.appState.tasks.countAllTasks
        val numberSelTasks = act.appState.tasks.countSelectedTasks
        return "${numberSelTasks} of ${numberAllTasks}"
    }

    private fun afterItemChanged() {
        val taskName = act.appState.tasks.currentTask.name
        act.appState.tasks.onTaskChanged()
        // notifyDataSetChanged needed here,
        // because change of task may cause the current task to be removed from the list,
        // or move the current task to another position within the list
        binding.taskListInFragment.adapter?.notifyDataSetChanged()
        binding.taskLstNumTasks.setText(textNumTasks())
        Toast.makeText(
            this.context,
            "changes for task \"${taskName}\" saved",
            Toast.LENGTH_LONG
        ).show()
        act.writeTasks()
    }

    private fun doSwipe(direction: FilterDirection) {
        when (act.appState.tasks.listMode) {
            ListMode.FILTERED_LIST ->  {
                if (act.appState.tasks.filters.dueDateFilter is SingleFilter1) {
                    if(canScroll(direction)) {
                        act.appState.tasks.filters.moveDueDateFilter(direction)
                        act.appState.tasks.refreshSelectedTasks()
                        binding.taskListInFragment.adapter?.notifyDataSetChanged()  // the tasks for the previous / next day / week are displayed
                        binding.taskLstHeader.setText(textHeader())
                        binding.taskLstNumTasks.setText(textNumTasks())
                    } else {
                        displayMessageNoScrolling(direction)
                    }
                }
            }
            ListMode.SINGLE_CATEGORY ->  {
                val oldCategory = act.appState.categories.currentItem
                act.appState.categories.moveCategory(direction)
                val newCategory = act.appState.categories.currentItem
                if (newCategory != oldCategory) {
                    act.appState.tasks.currentCategory = newCategory
                    act.appState.tasks.refreshSelectedTasks()
                    binding.taskListInFragment.adapter?.notifyDataSetChanged()  // the tasks for the previous / next category are displayed
                    binding.taskLstHeader.setText(textHeader())
                    binding.taskLstNumTasks.setText(textNumTasks())
                }
            }
            ListMode.STRING_SEARCH -> { // do nothing, in search mode no swiping
            }
        }
    }

    private fun canScrollBackward() : Boolean {
        val filterRangeStartDate = act.appState.tasks.filters.filterRangeStartDate
        // current filterRange is in future -> then it is always possible to scroll backward
        if (filterRangeStartDate > TDate.today) return true
        // currentFilterRange is in past
        // if there are no tasks with a due date, scrolling backward not possible
        val firstDueDate = act.appState.tasks.firstDueDate ?: return false
        // if there are tasks with a due date, scrolling backward possible when there are tasks before currentFilterRange
        return firstDueDate < filterRangeStartDate
    }
    private fun canScrollForward() : Boolean {
        val filterRangeEndDate = act.appState.tasks.filters.filterRangeEndDate
        // current filterRange is in past -> then it is always possible to scroll forward
        if (filterRangeEndDate < TDate.today) return true
        // currentFilterRange is in future
        // if there are no tasks with a due date, scrolling forward not possible
        val lastDueDate = act.appState.tasks.lastDueDate ?: return false
        // if there are tasks with a due date, scrolling forward possible when there are tasks past currentFilterRange
        return lastDueDate > filterRangeEndDate
    }

    private fun canScroll(direction: FilterDirection) : Boolean {
        val dueDateFilter = act.appState.tasks.filters.dueDateFilter
        return if (dueDateFilter is SingleFilter1) {
            when (dueDateFilter.option) {
                DateFilter.TODAY, DateFilter.CURRENT_WEEK  -> {
                    when (direction) {
                        FilterDirection.BACKWARD -> canScrollBackward()
                        FilterDirection.FORWARD -> canScrollForward()
                    }
                }
                else -> {
                    true
                }
            }
        }
        else false
    }

    private fun displayMessageNoScrolling(direction: FilterDirection) {
        val dueDateFilter = act.appState.tasks.filters.dueDateFilter
        if (dueDateFilter is SingleFilter1) {
            when (dueDateFilter.option) {
                DateFilter.TODAY, DateFilter.CURRENT_WEEK -> {
                    val textBeforeAfterDay = when (direction) {
                        FilterDirection.BACKWARD -> {
                            act.appState.tasks.firstDueDate?.let {
                                " before ${it.displayAs_MMM_DD}"
                            } ?: ""
                        }
                        FilterDirection.FORWARD -> {
                            act.appState.tasks.lastDueDate?.let {
                                " after ${it.displayAs_MMM_DD}"
                            } ?: ""
                        }
                    }
                    val textBackwardForward = when (direction) {
                        FilterDirection.BACKWARD -> {
                            act.appState.tasks.firstDueDate?.let {
                                "no more scrolling backward"
                            } ?: "no scrolling backward"
                        }
                        FilterDirection.FORWARD -> {
                            act.appState.tasks.lastDueDate?.let {
                                "no more scrolling forward"
                            } ?: "no scrolling forward"
                        }
                    }
                    val text = "no tasks with due date$textBeforeAfterDay,\n$textBackwardForward."
                    Toast.makeText(
                        this.context,
                        text,
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {}
            }
        }
    }

//    private fun getNextCategory(direction: FilterDirection) : Category{
//        val currentCategory = act.appState.categories.currentItem
//        while (true) {  // loop stops when next category with tasks was found, or when finally all categories have been tried and we come back to the currentCategory
//            act.appState.categories.moveCategory(direction)
//            val category = act.appState.categories.currentItem
//            if (act.appState.tasks.tasksExistingForCategory(category) || category == currentCategory) {
//                // return the next category that has tasks
//                // if that does not exist, return the currentCategory to avoid endless loop
//                return category
//            }
//        }
//    }

    private fun startIntentForImportDataRead() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.setType("text/xml")
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        importDataReadResultLauncher.launch(intent)
    }

    // importDataReadResultLauncher must be a member variable, a local variable inside startIntentForImportDataRead is not enough,
    // because importDataReadResultLauncher must be created during, or right after, creation of FragmentTaskLst
    private val importDataReadResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val intent = result.data
            val selectedFile: Uri? = intent?.data    //The uri with the location of the file
            Log.i("DEBUG", "URI of import file is $selectedFile")
            if (selectedFile != null) {
                val inputStream = act.contentResolver.openInputStream(selectedFile)
                if (inputStream != null) {
                    try {
                        act.appState.readAllDataFromStream(inputStream)
                        binding.taskListInFragment.adapter?.notifyDataSetChanged()
                        binding.taskLstHeader.setText(textHeader())
                        binding.taskLstNumTasks.setText(textNumTasks())
                        act.writeCategories()
                        act.writeTasks()
                        act.writeFilters()
                        act.writeSortCrit()
                        Toast.makeText(this.context, "imported ${act.appState.categories.countItems} categories and ${act.appState.tasks.countAllTasks} tasks ", Toast.LENGTH_LONG).show()
                    } catch (e: Exception) {
                        val message = "error importing data: $e"
                        Toast.makeText(this.context, message, Toast.LENGTH_LONG).show()
                        Log.i("DEBUG", message)
                    } finally {
                        inputStream.close()
                    }
                } else {
                    val message = "selected file $selectedFile could not be resolved to inputstream"
                    Toast.makeText(this.context, message, Toast.LENGTH_LONG).show()
                    Log.i("DEBUG", message)
                }
            } else {
                val message = "selected file $selectedFile does not exist"
                Toast.makeText(this.context, message, Toast.LENGTH_LONG).show()
                Log.i("DEBUG", message)
            }
        }
    }

//    private fun startIntentForFileReadFromTML() {
//        val intent = Intent(Intent.ACTION_GET_CONTENT)
//        intent.setType("text/xml")
//        intent.addCategory(Intent.CATEGORY_OPENABLE)
//        fileReadFromTMLResultLauncher.launch(intent)
//    }
//
//    private val fileReadFromTMLResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
//        if (result.resultCode == Activity.RESULT_OK) {
//            Toast.makeText(
//                act.applicationContext,
//                "fileReadFromTML launched and ok",
//                Toast.LENGTH_LONG).show()
//            val intent = result.data
//            val selectedFile: Uri? = intent?.data    //The uri with the location of the file
//            Log.i("DEBUG", "URI is $selectedFile")
//            if (selectedFile != null) {
//                val inputStream = act.contentResolver.openInputStream(selectedFile)
//                if (inputStream != null) {
//                    try {
//                        act.appState.readTMLiteDataFromStream(inputStream)
//                        binding.taskListInFragment.adapter?.notifyDataSetChanged()
//                        binding.taskLstHeader.setText(textHeader())
//                        binding.taskLstNumTasks.setText(textNumTasks())
//                        //  for test inactivated
////                            act.writeCategories()
////                            act.writeTasks()
////                            act.writeFilters()
////                            act.writeSortCrit()
//                        Toast.makeText(this.context, "imported ${act.appState.categories.countItems} categories and ${act.appState.tasks.countAllTasks} tasks from TML file", Toast.LENGTH_LONG).show()
//                    } catch (e: Exception) {
//                        Toast.makeText(this.context, "error importing TML data $e", Toast.LENGTH_LONG).show()
//                    } finally {
//                        inputStream.close()
//                    }
//                } else {
//                    Log.i("DEBUG", "selected file $selectedFile could not be resolved to inputstream")
//                }
//            } else {
//                Log.i("DEBUG", "selected file $selectedFile does not exist")
//            }
//        }
//    }

    private fun createDialogForSearch() {
        val dialogSearchBinding = DialogSearchBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this.context)
        builder
            .setMessage("Search for:")
            .setView(dialogSearchBinding.root)
            .setPositiveButton("Search") { dialog, id -> }
            .setNegativeButton("Cancel") { dialog, id -> }
        val dialog = builder.create()

        // makes the keyboard appear
        //dialog.window?.setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
        dialog.show()
        dialogSearchBinding.searchText.setText(act.appState.tasks.searchCrit.sstring)
        dialogSearchBinding.searchText.setSelection(0, dialogSearchBinding.searchText.text.length) // highlights the whole search text
        dialogSearchBinding.caseSensitive.isChecked = act.appState.tasks.searchCrit.caseSensitive
        dialogSearchBinding.nameDesc.isChecked = (act.appState.tasks.searchCrit.scopeOption == SearchScope.NAME_AND_DESC)
        dialogSearchBinding.descOnly.isChecked = (act.appState.tasks.searchCrit.scopeOption == SearchScope.DESC_ONLY)
        dialogSearchBinding.nameOnly.isChecked = (act.appState.tasks.searchCrit.scopeOption == SearchScope.NAME_ONLY)

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { view ->
            val searchString = dialogSearchBinding.searchText.text.toString().trim()
            if (searchString.isBlank()) {
                // show error message, and do not close
                dialogSearchBinding.searchMessage.text = "Search string must not be blank!"
            } else {
                val scopeOption = when (dialogSearchBinding.nameOrDesc.checkedRadioButtonId) {
                    dialogSearchBinding.nameOnly.id -> SearchScope.NAME_ONLY
                    dialogSearchBinding.descOnly.id -> SearchScope.DESC_ONLY
                    else -> SearchScope.NAME_AND_DESC
                }
                val caseSensitive = dialogSearchBinding.caseSensitive.isChecked
                // makes the keyboard disappear
                // dialog.window?.setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_HIDDEN)  // does not work
                imm?.hideSoftInputFromWindow(dialogSearchBinding.root.windowToken, 0)
                dialog.dismiss()

                // prepare showing results of string search, i.e. second level list
                act.appState.tasks.listModePending = ListMode.STRING_SEARCH
                act.appState.tasks.searchCrit = SearchCrit(searchString, caseSensitive, scopeOption)
                act.showTaskListSecondLevel()
            }
        }

        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener { view ->
            imm?.hideSoftInputFromWindow(dialogSearchBinding.root.windowToken, 0)
            dialog.dismiss()
        }
    }
}

class SwipeOnItemTouchListener(
    context: Context,
    // recyclerView: RecyclerView,
    private val mOnSwipeActionListener: OnSwipeActionListener
) : OnItemTouchListener {

    interface OnSwipeActionListener {
        fun onLeftSwipe()
        fun onRightSwipe()
    }

    companion object {
        private const val SWIPE_MIN_DISTANCE = 200
        private const val SWIPE_VELOCITY_THRESHOLD = 300
    }

    private val mGestureDetector: GestureDetectorCompat = GestureDetectorCompat(context, object : SimpleOnGestureListener() {
        // GestureDetectorCompat newer than GestureDetector

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            Log.i("DEBUG", "SimpleOnGestureListener onFling ${e1} ${e2}")
            var result = false
            try {
                val diffY = e2.y - e1.y
                val diffX = e2.x - e1.x
                if (abs(diffX) > abs(diffY)) {
                    // swipe horizontal
                    if (abs(diffX) > SWIPE_MIN_DISTANCE && abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
//                            Log.i("DEBUG", "SimpleOnGestureListener onFling onRightSwipe")
                            mOnSwipeActionListener.onRightSwipe()
                        } else {
//                            Log.i("DEBUG", "SimpleOnGestureListener onFling onLeftSwipe")
                            mOnSwipeActionListener.onLeftSwipe()
                        }
                        result = true  // todo better return false?
                    }
                    // swipe vertical not relevant here
                }
            } catch (exception: Exception) {
                Log.i("DEBUG", "SimpleOnGestureListener exception $exception")
                exception.printStackTrace()
            }
            return result
        }
    })

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        Log.i("DEBUG", "SwipeOnItemTouchListener onInterceptTouchEvent MotionEvent ${e}")
        mGestureDetector.onTouchEvent(e)  // todo  why is the mGestureDetector called here, not in onTouchEvent ?
        return false  // todo why return false, not true?
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        Log.i("DEBUG", "SwipeOnItemTouchListener onTouchEvent MotionEvent ${e}")
        // do nothing
    }
    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        // do nothing
    }
}

// helper extension function
// trick from https://gist.github.com/arcadefire/1e3a95314fdbdd78fb211b099d6ec9da
fun RecyclerView.addOnItemClickListener(onClick: (position: Int, view: View) -> Unit) {
    this.addOnChildAttachStateChangeListener(
        object : RecyclerView.OnChildAttachStateChangeListener {

            override fun onChildViewDetachedFromWindow(view: View) {
                view.setOnClickListener (null)
            }

            override fun onChildViewAttachedToWindow(view: View) {
                view.setOnClickListener {
                    val holder = getChildViewHolder(view)
                    onClick(holder.adapterPosition, view)
                }
            }
        }
    )
}
