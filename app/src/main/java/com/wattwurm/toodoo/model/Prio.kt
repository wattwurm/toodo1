package com.wattwurm.toodoo.model

import java.lang.Exception

enum class Prio(val rep: Int) {
    LOW(1),
    MEDIUM(2),
    HIGH(3);

    companion object {
        fun fromNumber(num: Int) = when (num) {
            1 -> LOW
            2 -> MEDIUM
            3 -> HIGH
            else -> throw Exception("Prio for number $num not existing")
        }
    }
}