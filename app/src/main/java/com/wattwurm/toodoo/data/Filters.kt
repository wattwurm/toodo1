package com.wattwurm.toodoo.data

import com.wattwurm.toodoo.model.Category
import com.wattwurm.toodoo.model.CompletionStatus
import com.wattwurm.toodoo.model.Prio
import com.wattwurm.toodoo.model.TDate
import com.wattwurm.toodoo.model.TTime
import com.wattwurm.toodoo.model.Task

class Filters {

    var completionFilter : SingleFilter<CompletionStatus> = FilterAll
    var dueDateFilter : SingleFilter<DateFilter> = FilterAll
        set(newFilter) {
            if (this.dueDateFilter != newFilter) {
                field = newFilter
                //  when dueDateFilter changes, startDate must be rest to null
                startDate = null
            }
        }
    var prioFilter : MultipleFilter<Prio> = FilterAll
    var catFilter : MultipleFilter<Category> = FilterAll

    // only used for dueDateFilter TODAY and CURRENT_WEEK
    private var startDate: TDate? = null

    fun useCategory(cat: Category) : Boolean {
        catFilter.let {
            return (it is MultipleFilterM && it.options.contains(cat))
        }
    }

    fun applyToTask(task: Task) : Boolean {
        if (!this.dueDateFilterIncludes(task)) return false
        completionFilter.let {
            if (it is SingleFilter1) {
                if(task.status != it.option) return false
            }
        }
        prioFilter.let {
            if (it is MultipleFilterM) {
                if(!(it.options.contains(task.priority))) return false
            }
        }
        catFilter.let {
            if (it is MultipleFilterM) {
                if(!(it.options.contains(task.category))) return false
            }
        }
        return true
    }

    val displayInOverview: String get() {
        fun combine (str1: String, str2: String) = if (str1 == "") str2 else if (str2 == "") str1 else "${str1} & ${str2}"
        var result = ""
        dueDateFilter.let { if(it is SingleFilter1) result = combine(result, this.displayDuedateFilterInOverview) }
        completionFilter.let { if(it is SingleFilter1) result = combine(result, it.display) }
        prioFilter.let { if(it is MultipleFilterM) result = combine(result, it.displayInOverview) }
        catFilter.let { if(it is MultipleFilterM) result = combine(result, it.displayInOverview) }
        return result
    }

    override fun equals(other: Any?): Boolean {
        return if (other is Filters)
            this.completionFilter == other.completionFilter
                    && this.dueDateFilter == other.dueDateFilter
                    && this.prioFilter == other.prioFilter
                    && this.catFilter == other.catFilter
        else false
    }

    override fun hashCode(): Int {
        return ((completionFilter.hashCode() * 31 + dueDateFilter.hashCode()) * 31 + prioFilter.hashCode()) * 31 + catFilter.hashCode()
    }

    fun copy(): Filters {
        val copy = Filters()
        copy.completionFilter = this.completionFilter
        copy.dueDateFilter = this.dueDateFilter
        copy.prioFilter = this.prioFilter
        copy.catFilter = this.catFilter
        // do NOT copy startDate
        return copy
    }

    fun resetToDefault() {
        completionFilter = FilterAll
        dueDateFilter  = FilterAll
        prioFilter = FilterAll
        catFilter = FilterAll
    }

    private fun dueDateFilterIncludes(task: Task): Boolean {
        dueDateFilter.let { filter ->
            if (filter is SingleFilter1) {
                val date = task.dueDate
                val time = task.dueTime
                val today = TDate.today
                val now = TTime.now
                val dateFilter = filter.option
                return if (date == null)
                    dateFilter == DateFilter.TASKS_WITHOUT_DATE
                else
                    when (dateFilter) {
                        DateFilter.TODAY -> {
                            if (startDate == null) date == today
                            else date == startDate
                        }
                        DateFilter.CURRENT_WEEK -> {
                            startDate.let {
                                if (it == null) date.isInCurrentWeek()
                                else it.addDays(-1).isLessThan(date) && date.isLessThan(it.addDays(7))
                            }
                        }
                        DateFilter.OVERDUE -> {
                            // due time may be relevant
                            date.isLessThan(today) || (date == today && time != null && time.compareTo(now) <= 0)
                        }
                        DateFilter.NEXT_4_WEEKS -> {
                            (today.isLessThan(date) && date.isLessThan(today.addDays(28)))
                                    || (date == today && time == null)
                                    || (date == today && time != null && time.compareTo(now) > 0)
                        }
                        DateFilter.MORE_THAN_4_WEEKS -> {
                            today.addDays(27).isLessThan(date)
                        }
                        DateFilter.TASKS_WITHOUT_DATE -> {
                            false
                        }
                    }
            } else {
                return true
            }
        }
    }

//    fun filterDateIsBeforeInAfterDateRange(dateFrom: TDate, dateTo: TDate) : Int {
//        dueDateFilter.let { filter ->
//            if (filter is SingleFilter1) {
//                val dateFilter = filter.option
//                when (dateFilter) {
//                    DateFilter.TODAY -> {
//                        val currentStartDate = startDate ?: TDate.today
//                        if(currentStartDate.isLessThan(dateFrom)) return -1
//                        if(dateTo.isLessThan(currentStartDate)) return 1
//                        return 0
//                    }
//                    DateFilter.CURRENT_WEEK -> {
//                        val currentStartDate = startDate ?: TDate.today.mondayBefore()
//                        if(currentStartDate.isLessThan(dateFrom.mondayBefore())) return -1
//                        if(dateTo.mondayBefore().isLessThan(currentStartDate)) return 1
//                        return 0
//                    }
//                    else -> {
//                        return 0
//                    }
//                }
//            }
//        }
//        return 0
//    }

    val filterRangeStartDate: TDate
        get() {
        dueDateFilter.let { filter ->
            if (filter is SingleFilter1) {
                return when (filter.option) {
                    DateFilter.TODAY -> {
                        startDate ?: TDate.today
                    }
                    DateFilter.CURRENT_WEEK -> {
                        startDate ?: TDate.today.mondayBefore()
                    }
                    else -> {
                        TDate.today
                    }
                }
            }
        }
        return TDate.today
    }

    val filterRangeEndDate: TDate
        get() {
        dueDateFilter.let { filter ->
            if (filter is SingleFilter1) {
                return when (filter.option) {
                    DateFilter.TODAY -> {
                        startDate ?: TDate.today
                    }
                    DateFilter.CURRENT_WEEK -> {
                        (startDate ?: TDate.today.mondayBefore()).addDays(6)
                    }
                    else -> {
                        TDate.today
                    }
                }
            }
        }
        return TDate.today
    }

    fun moveDueDateFilter(direction: FilterDirection) {
        dueDateFilter.let { filter ->
            if (filter is SingleFilter1) {
                val directionNum = when (direction) {
                    FilterDirection.FORWARD -> 1
                    FilterDirection.BACKWARD -> -1
                }
                val dateFilter = filter.option
                when (dateFilter) {
                    DateFilter.TODAY -> {
                        val date = startDate ?: TDate.today
                        startDate = date.addDays(directionNum)
                    }
                    DateFilter.CURRENT_WEEK -> {
                        val date = (startDate ?: TDate.today.mondayBefore())
                        startDate = date.addDays(directionNum * 7)
                    }
                    else -> {
                        dueDateFilter = SingleFilter1(when (direction) {
                            FilterDirection.FORWARD -> dateFilter.next
                            FilterDirection.BACKWARD -> dateFilter.previous
                        })
                    }
                }
            }
        }
    }

    // used in FragmentTaskLst in the header
    private val displayDuedateFilterInOverview: String get() {
        dueDateFilter.let { filter ->
            if (filter is SingleFilter1) {
                val dateFilter = filter.option
                return when (dateFilter) {
                    // for DAY and WEEK take into consideration startDate
                    DateFilter.TODAY -> {
                        startDate.let {
                            when (it) {
                                null -> "TODAY"
                                TDate.today -> "TODAY"
                                TDate.today.addDays(1) -> "TOMORROW"
                                TDate.today.addDays(-1) -> "YESTERDAY"
                                else -> "DAY ${it.displayAs_MMM_DD}"
                            }
                        }
                    }
                    DateFilter.CURRENT_WEEK -> {
                        startDate.let {
                            when {
                                it == null -> "CURRENT_WEEK"
                                it.isInCurrentWeek() -> "CURRENT_WEEK"
                                it.addDays(-7).isInCurrentWeek() -> "NEXT_WEEK"
                                it.addDays(7).isInCurrentWeek() -> "LAST_WEEK"
                                else -> "WEEK ${it.weekOfYear}"
                            }
                        }
                    }
                    else -> {
                        dateFilter.name
                    }
                }
            } else {
                return ""
            }
        }
    }

    val defaultDateForNewTask: TDate
        get() {
        var result = TDate.today
        dueDateFilter.let { filter ->
            if (filter is SingleFilter1) {
                when (filter.option) {
                    // for DAY and WEEK take into consideration startDate
                    DateFilter.TODAY -> {
                        startDate.let {
                            if (it != null) {
                                result = it
                            }
                        }
                    }
                    DateFilter.CURRENT_WEEK -> {
                        startDate.let {
                            if (it != null && !it.isInCurrentWeek()) {
                                result = it
                            }
                        }
                    }
                    else -> {}
                }
            }
        }
        return result
    }
}

enum class DateFilter {
    TODAY, CURRENT_WEEK, OVERDUE, NEXT_4_WEEKS, MORE_THAN_4_WEEKS, TASKS_WITHOUT_DATE;

    val next get() = when (this) {
        OVERDUE -> NEXT_4_WEEKS
        NEXT_4_WEEKS -> MORE_THAN_4_WEEKS
        MORE_THAN_4_WEEKS -> TASKS_WITHOUT_DATE
        TASKS_WITHOUT_DATE -> OVERDUE
        else -> this
    }
    val previous get() = when (this) {
        OVERDUE -> TASKS_WITHOUT_DATE
        NEXT_4_WEEKS -> OVERDUE
        MORE_THAN_4_WEEKS -> NEXT_4_WEEKS
        TASKS_WITHOUT_DATE -> MORE_THAN_4_WEEKS
        else -> this
    }

}

enum class FilterDirection {
    FORWARD, BACKWARD;
}

interface SingleFilter<out T> {  // out is needed here, otherwise FilterAll does not count as a subtype of SingleFilter<out T>
    val display : String
}

data class SingleFilter1<T> (val option: T) : SingleFilter<T> {
    override val display: String get() = option.toString()
}

interface MultipleFilter<out T> {
    val display : String
}

data class MultipleFilterM<T: Comparable<T>> (val options: List<T>) : MultipleFilter<T> {
    override val display: String get() = options.toString()
    private fun combine (str1: String, str2: String) = if (str1 == "") str2 else if (str2 == "") str1 else "${str1}|${str2}"
    val displayInOverview: String get() = options.fold ("") { e1, e2 -> combine(e1, e2.toString()) }
}

object FilterAll : SingleFilter<Nothing>, MultipleFilter<Nothing> {  // type Nothing is needed here, otherwise FilterAll does not count as a subtype of SingleFilter and MultipleFilter
    override val display: String get() = "[ ALL ]"
}
