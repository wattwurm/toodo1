package com.wattwurm.toodoo.data

import com.wattwurm.toodoo.model.Category
import java.lang.Exception

class Categories {
    private val catList = mutableListOf<Category>()

    var currentCatPos = -1

    private var nextId = 0
    private fun getNextId() : Int {
        nextId++
        return nextId
    }
    fun resetNextId() {
        nextId = 0
    }

    fun createNewCategory(name: String): Category {
        return Category(getNextId(), name)
    }

    fun categoryForId(id: Int): Category {
        for (cat in catList ) {
            if (cat.id == id) return cat
        }
        throw Exception("category with id $id not existing")
    }

    fun categoryForName(name: String): Category {
        for (cat in catList ) {
            if (cat.name == name) return cat
        }
        throw Exception("category with name $name not existing")
    }

    fun containsName(name: String): Boolean {
        for (cat in catList ) {
            if (cat.name.equals(name, ignoreCase = true)) return true
        }
        return false
    }

    val countItems get() = catList.size

    fun itemAtPosition(pos: Int): Category {
        return catList[pos]
    }

    // precondition: currentCatPos > -1
    val currentItem: Category
        get() {
            return catList[currentCatPos]
        }

    fun removeCurrentItem() {
        catList.removeAt(currentCatPos)
        // remove of category is safe:
        //  before deleting, FragmentCategories checks that category is not used in tasks or filters
    }

    fun addCategoryWithName(name: String) {
        catList.add(this.createNewCategory(name))
        sortCategories()
    }

    fun renameCurrentItem(name: String) {
        val cat = catList[currentCatPos]
        cat.name = name
        sortCategories()
    }

    private fun sortCategories() {
        catList.sort()
    }

    fun setCategoriesFromStorage(categories: List<Category>) {
        catList.clear()
        catList.addAll(categories)
        sortCategories()
        nextId = catList.map { it.id }.maxOrNull() ?: 0
    }

    fun moveCategory(direction: FilterDirection) {
        currentCatPos = when (direction) {
            FilterDirection.BACKWARD -> {
                if(currentCatPos <= 0) countItems - 1
                else currentCatPos - 1
            }
            FilterDirection.FORWARD -> {
                if(currentCatPos >= countItems - 1) 0
                else currentCatPos + 1
            }
        }
    }

    val iterator get() = catList.iterator()

    val allCategoryNames get() = catList.map { it.name }

}
