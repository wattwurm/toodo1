package com.wattwurm.toodoo.data

import android.util.Log
import com.wattwurm.toodoo.model.Category
import com.wattwurm.toodoo.model.CompletionStatus
import com.wattwurm.toodoo.model.Prio
import com.wattwurm.toodoo.model.TDate
import com.wattwurm.toodoo.model.TTime
import com.wattwurm.toodoo.model.Task
import com.wattwurm.toodoo.model.convertStringFromXMLToDate
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.InputStream
import java.util.Calendar
import java.util.Date

//    code for reading AppState (tasks, categories, filters, sortCriteria),
//    either at startup (from internal storage), or on user request (import data)

    fun AppState.readCategoriesFromStream(inputStream: InputStream) {
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)
        val categoryList = readCategoriesFromParser(parser)
        this.categories.setCategoriesFromStorage(categoryList)
    }

    private fun readCategoriesFromParser(parser: XmlPullParser) : List<Category> {
        val categoryList = mutableListOf<Category>()
        var eventType = parser.eventType
        var counter = 0
        while (!(eventType == XmlPullParser.END_TAG && parser.name == "categories") && eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if(parser.name == "category") {
                    val catId = parser.getAttributeValue(null, "id").toInt()
                    val catName = parser.getAttributeValue(null, "name")
                    val category = Category(catId, catName)
                    categoryList.add(category)
                    counter++
                }
            }
            eventType = parser.next()
        }
        Log.i("DEBUG", "$counter categories read from xml")
        return categoryList
    }

    fun AppState.readTasksFromStream(inputStream: InputStream) {
        Log.i("DEBUG", "readTasksFromStream start")
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)
        val taskList = readTasksFromParser(parser)

        // for each task, replace dummy category with matching category
        taskList.forEach { it.category = this.categories.categoryForId(it.category.id) }
        this.tasks.setTasksFromStorage(taskList)
        Log.i("DEBUG", "readTasksFromStream end")
    }

    private fun readTasksFromParser(parser: XmlPullParser) : List<Task> {
        val taskList = mutableListOf<Task>()
        var task = Task()
        var descTag = false
        var counter = 0

        var eventType = parser.eventType
        while (!(eventType == XmlPullParser.END_TAG && parser.name == "tasks") && eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if(parser.name == "task") {
                    task = Task()
                    task.id = parser.getAttributeValue(null, "id").toInt()
                    task.name = parser.getAttributeValue(null, "name")
                    task.priority = Prio.fromNumber(parser.getAttributeValue(null, "prio").toInt())
                    task.status = CompletionStatus.fromSymbol(parser.getAttributeValue(null, "comp").toCharArray()[0])
                    task.dueDate = parser.getAttributeValue(null, "ddate")?.let { TDate.fromStorageRep(it) }
                    task.dueTime = parser.getAttributeValue(null, "dtime")?.let { TTime.fromStorageRep(it) }
                    convertStringFromXMLToDate(parser.getAttributeValue(null, "cdate"))?.let { task.createDate.time = it }
                    convertStringFromXMLToDate(parser.getAttributeValue(null, "mdate"))?.let { task.lastModifiedDate.time = it }
                    task.category = Category(parser.getAttributeValue(null, "cat").toInt(), "")
                } else if (parser.name == "desc") {
                    descTag = true
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if(parser.name == "task") {
                    taskList.add(task)
                    Log.i("DEBUG", "task read from stream: ${task.displayInLog()}")
                    task.desc.let { if(it.isNotEmpty()) { Log.i("DEBUG", "task desc: ${it}")} }
                    counter++
                } else if (parser.name == "desc") {
                    descTag = false
                }
            } else if (eventType == XmlPullParser.TEXT) {
                if (descTag) {
                    // Log.i("DEBUG", "xml parser text in desc: ${parser.text}")
                    task.desc = parser.text
                }
            }
            eventType = parser.next()
        }
        Log.i("DEBUG", "$counter tasks read from xml")
        return taskList
    }

    fun AppState.readFiltersFromStream(inputStream: InputStream) {
        Log.i("DEBUG", "readFiltersFromStream start")
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)
        val filters = readFiltersFromParser(parser)

        // for catFilter, replace dummy categories with matching category, if necessary
        filters.catFilter.let { catFilter ->
            if(catFilter is MultipleFilterM<Category>) {
                val validCategories = catFilter.options.map { categories.categoryForName(it.name) }
                filters.catFilter = MultipleFilterM(validCategories)
            }
        }
        tasks.filters = filters
        tasks.refreshSelectedTasks()
        Log.i("DEBUG", "readFiltersFromStream end")
    }

    private fun readFiltersFromParser(parser: XmlPullParser) : Filters {
        var crit = ""
        var type = ""
        var option = ""
        val options = mutableListOf<String>()

        val filters = Filters()
        var eventType = parser.eventType

        while (!(eventType == XmlPullParser.END_TAG && parser.name == "filters") && eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if (parser.name == "filter") {
                    crit = parser.getAttributeValue(null, "crit")
                    type = parser.getAttributeValue(null, "type")
                } else if (parser.name == "option") {
                    val optionName = parser.getAttributeValue(null, "name")
                    if (type == "single") {
                        option = optionName
                    } else if (type == "mult") {
                        options.add(optionName)
                    }
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.name == "filter") {
                    when (crit) {
                        SortCrit.Completed.name -> {
                            filters.completionFilter =
                                if (type == "single") SingleFilter1(CompletionStatus.valueOf(option)) else FilterAll
                        }
                        SortCrit.DueDate.name -> {
                            filters.dueDateFilter =
                                if (type == "single") SingleFilter1(DateFilter.valueOf(option)) else FilterAll
                        }
                        SortCrit.Priority.name -> {
                            filters.prioFilter =
                                if (type == "mult") MultipleFilterM(options.map { Prio.valueOf(it) }) else FilterAll
                        }
                        SortCrit.Category.name -> {
                            filters.catFilter =
                                if (type == "mult") MultipleFilterM(options.map { Category(0, it) }) else FilterAll
                        }
                    }
                    options.clear()
                }
            }
            eventType = parser.next()
        }
        Log.i("DEBUG", "filters read from xml")
        return filters
    }

    fun AppState.readSortCritFromStream(inputStream: InputStream) {
        Log.i("DEBUG", "readSortCritFromStream start")
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)

        val sortCrit = readSortCritFromParser(parser)
        tasks.setSortOrder(sortCrit)
        Log.i("DEBUG", "readSortCritFromStream end")
    }

    private fun readSortCritFromParser(parser: XmlPullParser): List<SortCrit> {
        val sortCrit = mutableListOf<SortCrit>()
        var orderBefore = 0
        var eventType = parser.eventType
        while (!(eventType == XmlPullParser.END_TAG && parser.name == "sorts") && eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if(parser.name == "sort") {
                    val order = parser.getAttributeValue(null, "order").toInt()
                    if (order == orderBefore + 1) {
                        val crit = SortCrit.valueOf(parser.getAttributeValue(null, "crit"))
                        sortCrit.add(crit)
                        orderBefore = order
                    } else {
                        throw Exception("import failed. sort order not consistent, $order following $orderBefore")
                    }
                }
            }
            eventType = parser.next()
        }
        Log.i("DEBUG", "sort criteria read from xml")
        return sortCrit
    }

    fun AppState.readAllDataFromStream(inputStream: InputStream) {
        Log.i("DEBUG", "readAllDataFromStream start")

        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)

        moveParserToTag(parser, "categories")
        val categoriesFromXML = readCategoriesFromParser(parser)
        if (categoriesFromXML.isEmpty()) {
            throw Exception("import failed. 0 categories in input")
        }

        moveParserToTag(parser, "tasks")
        val tasksFromXml = readTasksFromParser(parser)
        // for each task, replace dummy category with matching category
        tasksFromXml.forEach { task ->
            task.category = categoriesFromXML.firstOrNull { it.id == task.category.id } ?: throw Exception("import failed. task ${task.name} - no category with id ${task.category.id} found")
        }

        moveParserToTag(parser, "filters")
        val filtersFromXML = readFiltersFromParser(parser)
        // for catFilter, it is necessary to replace each dummy category with matching category
        filtersFromXML.catFilter.let { catFilter ->
            if(catFilter is MultipleFilterM<Category>) {
                val validCategories = catFilter.options.map { category ->
                    categoriesFromXML.firstOrNull { it.name == category.name } ?: throw Exception("import failed. for category filter, category with name ${category.name} not found")
                    }
                filtersFromXML.catFilter = MultipleFilterM(validCategories)
            }
        }

        moveParserToTag(parser, "sorts")
        val sortCritFromXML = readSortCritFromParser(parser)
        if (sortCritFromXML.size != SortCrit.defaultSortOrder.size) {
            throw Exception("import failed. sort criteria in input inconsistent,\n${sortCritFromXML.size} sort criteria found, ${SortCrit.defaultSortOrder.size} sort criteria expected")
        }

        this.categories.setCategoriesFromStorage(categoriesFromXML)
        this.tasks.setTasksFromStorage(tasksFromXml)
        this.tasks.filters = filtersFromXML
        this.tasks.refreshSelectedTasks()
        this.tasks.setSortOrder(sortCritFromXML)
        Log.i("DEBUG", "readAllDataFromStream end")
    }

    private fun moveParserToTag(parser: XmlPullParser, tag: String) {
        var eventType = parser.eventType
        while (!(eventType == XmlPullParser.START_TAG && parser.name == tag) && eventType != XmlPullParser.END_DOCUMENT) {
            eventType = parser.next()
        }
        if (!(eventType == XmlPullParser.START_TAG && parser.name == tag)) {
            throw Exception("import failed. $tag not found in input.")
        }
    }

    fun AppState.readTMLiteDataFromStream(inputStream: InputStream) {
        Log.i("DEBUG", "readTMLiteDataFromStream start")
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)

        val categoryList = mutableListOf<Category>()
        val taskList = mutableListOf<Task>()
        var taskTag = false
        var descTag = false
        var task = Task()
        this.tasks.resetNextId()
        this.categories.resetNextId()

        var eventType = parser.eventType
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if (parser.name == "task") {
                    task = Task()
                    task.id = this.tasks.getNextId()

                    val prioTML = parser.getAttributeValue(null, "priority").toInt()
                    task.priority = when (prioTML) {
                        4 -> Prio.LOW
                        3 -> Prio.MEDIUM
                        2 -> Prio.HIGH
                        else -> throw Exception("invalid priority ${prioTML} in file from TMLcategory")
                    }

                    val statusTML = parser.getAttributeValue(null, "status")
                    task.status = if (statusTML == "Completed") CompletionStatus.DONE else CompletionStatus.OPEN

                    // val due_date = Instant.ofEpochMilli(parser.getAttributeValue(null, "due_date").toLong())  // requires API level 26 or greater
                    val dueDateTML = parser.getAttributeValue(null, "due_date")
                    if(dueDateTML != null) {
                        val calenderD = Calendar.getInstance()
                        calenderD.time = Date(dueDateTML.toLong())

                        val year = calenderD.get(Calendar.YEAR).toShort()
                        val month = (calenderD.get(Calendar.MONTH) + 1).toByte()
                        val day = calenderD.get(Calendar.DAY_OF_MONTH).toByte()
                        task.dueDate = TDate(year,month,day)

                        val hour = calenderD.get(Calendar.HOUR_OF_DAY)
                        val minute = calenderD.get(Calendar.MINUTE)
                        // in TMLite, times 23:00 and 00:00 are often used to signal that no time is wanted at all (which TMLite cannot express explicitly)
                        task.dueTime = if((hour == 23 || hour == 0) && minute == 0) null else TTime(hour.toByte(), minute.toByte())
                    }

                    val creationDateTML = parser.getAttributeValue(null, "creation_date")
                    task.createDate.time = Date(creationDateTML.toLong())

                    val modifiedDateTML = parser.getAttributeValue(null, "modified_date")
                    task.lastModifiedDate.time = Date(modifiedDateTML.toLong())

                    val categoryTML = parser.getAttributeValue(null, "category")

                    val categoryForTask = categoryList.firstOrNull { it.name == categoryTML }
                    if (categoryForTask != null) {
                        task.category = categoryForTask
                    } else {
                        val newCategory = this.categories.createNewCategory(categoryTML)
                        categoryList.add(newCategory)
                        task.category = newCategory
                        Log.i("DEBUG", "new category created: ${newCategory.displayInLog()}")
                    }
                    taskTag = true

                } else if (parser.name == "description") {
                    descTag = true
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.name == "task") {
                    taskTag = false
                    taskList.add(task)
                    Log.i("DEBUG", "task read from stream: ${task.displayInLog()}")
                    task.desc.let { if(it.isNotEmpty()) { Log.i("DEBUG", "task desc: ${it}")} }
                } else if (parser.name == "description") {
                    descTag = false
                }
            } else if (eventType == XmlPullParser.TEXT) {
                val text = parser.text
                if (taskTag) {
                    if (descTag) {
                        task.desc = text
                    } else {
                        task.name = text
                    }
                }
            }
            eventType = parser.next()
        }
        Log.i("DEBUG", "${taskList.size} tasks read from TML xml")
        Log.i("DEBUG", "${categoryList.size}  categories read from TML  xml")
        if (categoryList.isNotEmpty() || taskList.isNotEmpty() ) {
            this.categories.setCategoriesFromStorage(categoryList)
            this.tasks.setTasksFromStorage(taskList)
            this.tasks.filters.resetToDefault()
            this.tasks.refreshSelectedTasks()
            this.tasks.setSortOrder(SortCrit.defaultSortOrder)
        } else {
            throw Exception("no categories or tasks found in TML file, nothing imported")
        }
        Log.i("DEBUG", "readTMLiteDataFromStream end")
    }
