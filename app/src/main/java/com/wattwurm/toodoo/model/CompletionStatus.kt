package com.wattwurm.toodoo.model

import java.lang.Exception

enum class CompletionStatus(val symbol: Char) {
    OPEN('O'),
    DONE('C');

    companion object {
        fun fromSymbol(symbol: Char) = when (symbol) {
            'O' -> OPEN
            'C' -> DONE
            else -> throw Exception("Status for symbol $symbol not existing")
        }
    }
}