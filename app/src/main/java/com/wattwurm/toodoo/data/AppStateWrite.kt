package com.wattwurm.toodoo.data

import android.util.Log
import android.util.Xml
import com.wattwurm.toodoo.model.convertCalndrForXML
import org.xmlpull.v1.XmlSerializer
import java.io.OutputStream

//    code for writing AppState (tasks, categories, filters, sortCriteria),
//    either to internal storage (after a change was done by user), or on user request (export data)

    fun AppState.writeAllDataToStream(stream: OutputStream) {
        createSerializerAndWriteData(stream) {serializer ->
            serializer.startTag(null, "root")
            writeCategoriesToSerializer(serializer)
            writeTasksToSerializer(serializer)
            writeFiltersToSerializer(serializer)
            writeSortCritToSerializer(serializer)
            serializer.endTag(null, "root")
        }
    }

    fun AppState.writeCategoriesToStream(stream: OutputStream) {
        createSerializerAndWriteData(stream) { serializer ->
            writeCategoriesToSerializer(serializer)
        }
    }

    private fun AppState.writeCategoriesToSerializer(serializer: XmlSerializer) {
        var counter = 0
        with(serializer) {
            startTag(null, "categories")
            for (category in categories.iterator) {
                startTag(null, "category")
                attribute(null, "id", category.id.toString())
                attribute(null, "name", category.name)
                endTag(null, "category")
                counter++
            }
            endTag(null, "categories")
        }
        Log.i("DEBUG", "done: $counter categories written as xml")
    }

    fun AppState.writeTasksToStream(stream: OutputStream) {
        createSerializerAndWriteData(stream) { serializer ->
            writeTasksToSerializer(serializer)
        }
    }

    private fun AppState.writeTasksToSerializer(serializer: XmlSerializer) {
        var count = 0
        with (serializer) {
            startTag(null, "tasks")
            for (task in tasks.iterator) {
                startTag(null, "task")
                attribute(null, "id", task.id.toString())
                attribute(null, "name", task.name)
                attribute(null, "prio", task.priority.rep.toString())
                attribute(null, "comp", task.status.symbol.toString())
                task.dueDate?.let { attribute(null, "ddate", it.toStorageRep()) }   // skip if null
                task.dueTime?.let { attribute(null, "dtime", it.toStorageRep()) }   // skip if null
                attribute(null, "cat", task.category.id.toString())
                attribute(null, "cdate", convertCalndrForXML(task.createDate))
                attribute(null, "mdate", convertCalndrForXML(task.lastModifiedDate))
                if(task.desc.isNotEmpty()) {  //  tag for description only if not empty
                    startTag(null, "desc")
                    text(task.desc)
                    endTag(null, "desc")
                }
                endTag(null, "task")
                count++
            }
            endTag(null, "tasks")
        }
        Log.i("DEBUG", "done: $count tasks written as xml")
    }

    fun AppState.writeFiltersToStream(stream: OutputStream) {
        createSerializerAndWriteData(stream) { serializer ->
            writeFiltersToSerializer(serializer)
        }
    }

    private fun AppState.writeFiltersToSerializer(serializer: XmlSerializer) {
        val filters = tasks.filters
        serializer.startTag(null, "filters")
        writeSingleFilterToSerializer(serializer, SortCrit.Completed.name, filters.completionFilter)
        writeSingleFilterToSerializer(serializer, SortCrit.DueDate.name, filters.dueDateFilter)
        writeMultipleFilterToSerializer(serializer, SortCrit.Priority.name, filters.prioFilter)
        writeMultipleFilterToSerializer(serializer, SortCrit.Category.name, filters.catFilter)
        serializer.endTag(null, "filters")
        Log.i("DEBUG", "done: filters written as xml")
    }

    private fun <T> writeSingleFilterToSerializer(serializer: XmlSerializer, crit: String, filter: SingleFilter<T>) {
        with (serializer) {
            startTag(null, "filter")
            attribute(null, "crit", crit)
            val type = when (filter) {
                is FilterAll -> "all"
                is SingleFilter1 -> "single"
                else -> "dummy"  // not possible, can only be FilterAll or SingleFilter1
            }
            attribute(null, "type", type)
            if (filter is SingleFilter1) {
                startTag(null, "option")
                attribute(null, "name", filter.option.toString())
                endTag(null, "option")
            }
            endTag(null, "filter")
        }
    }

    private fun <T> writeMultipleFilterToSerializer(serializer: XmlSerializer, crit: String, filter: MultipleFilter<T>) {
        with (serializer) {
            startTag(null, "filter")
            attribute(null, "crit", crit)
            val type = when (filter) {
                is FilterAll -> "all"
                is MultipleFilterM -> "mult"
                else -> "dummy"  // not possible, can only be FilterAll or MultipleFilterM
            }
            attribute(null, "type", type)
            if (filter is MultipleFilterM) {
                val options = filter.options
                options.forEach {
                    startTag(null, "option")
                    attribute(null, "name", it.toString())
                    endTag(null, "option")
                }
            }
            endTag(null, "filter")
        }
    }

    fun AppState.writeSortCritToStream(stream: OutputStream) {
        createSerializerAndWriteData(stream) { serializer ->
            writeSortCritToSerializer(serializer)
        }
    }

    private fun AppState.writeSortCritToSerializer(serializer: XmlSerializer) {
        serializer.startTag(null, "sorts")
        tasks.sortOrder.forEachIndexed { index, sortCrit ->
            serializer.startTag(null, "sort")
            serializer.attribute(null, "order", (index+1).toString())
            serializer.attribute(null, "crit", sortCrit.toString())
            serializer.endTag(null, "sort")
        }
        serializer.endTag(null, "sorts")
        Log.i("DEBUG", "done: sort criteria written as xml")
    }

    private fun createSerializerAndWriteData(stream: OutputStream, writeToSerializer: (XmlSerializer) -> Unit ) {
        Log.i("DEBUG", "writing data to stream started ")
        val serializer: XmlSerializer = Xml.newSerializer()
        serializer.setOutput(stream, "UTF-8")
        serializer.startDocument(null, true)
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true)
        writeToSerializer(serializer)
        serializer.endDocument()
        serializer.flush()
        Log.i("DEBUG", "writing data to stream finished")
    }
